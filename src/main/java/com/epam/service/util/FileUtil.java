package com.epam.service.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {
    private static Logger log = LogManager.getLogger(FileUtil.class);
    private static final String PATH = "text.txt";

    public static String readText() {
        String text = "";
        try {
            text = new String(Files.readAllBytes(Paths.get(PATH)));
        } catch (IOException e) {
            log.warn("File not found!");
        }
        return text;
    }
}
