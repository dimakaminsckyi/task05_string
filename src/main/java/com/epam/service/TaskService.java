package com.epam.service;

public interface TaskService {

    void countSentenceRepeatWord();

    void showSentenceByWordNumber();

    void interrogativeSentence(int wordLength);

    void replaceWordForLongestWord();

    void deleteWordByLength(int length);

    void findUniqueWordInFirstSentence();

    void sortWordsByAlphabet();

    void sortVowelWordByConsonants();

    void sortByLetterCount(char letter);

    void sortByLetterCountReverse(char letter);

    void swapWordInSentence(int sentenceNumber, int wordLength, String newWord);

    void deleteFirstLastLetterFromWord();

    void deleteSubstringBy(char startChar, char endChar);

    void sortWordsByVowelPercent();
}
