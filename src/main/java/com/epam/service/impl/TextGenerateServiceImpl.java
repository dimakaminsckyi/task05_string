package com.epam.service.impl;

import com.epam.service.TextGenerateService;
import com.epam.model.Sentence;
import com.epam.model.Text;
import com.epam.model.Word;
import com.epam.service.util.FileUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextGenerateServiceImpl implements TextGenerateService {

    @Override
    public Text generateText() {
        Text text;
        text = new Text(FileUtil.readText());
        text.setSentencesList(generateSentenceList(text.getText()));
        text.setWordsList(generateWordList(text.getText()));
        text.getSentencesList().forEach(s -> s.setWordList(generateWordList(s.getSentence())));
        return text;
    }

    private List<Sentence> generateSentenceList(String textString) {
        final String sentenceRegex = "[^.?!]+[.?!]";
        List<Sentence> sentenceList = new ArrayList<>();
        Pattern pattern = Pattern.compile(sentenceRegex);
        Matcher matcher = pattern.matcher(textString);
        while (matcher.find()) {
            sentenceList.add(new Sentence(matcher.group()));
        }
        return sentenceList;
    }

    private List<Word> generateWordList(String text) {
        final String wordRegex = "([^\\s,.!?]+)";
        List<Word> wordList = new ArrayList<>();
        Pattern pattern = Pattern.compile(wordRegex);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            wordList.add(new Word(matcher.group()));
        }
        return wordList;
    }
}
