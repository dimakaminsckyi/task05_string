package com.epam.controller;

import com.epam.service.TaskService;
import com.epam.service.impl.TaskServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class TaskController {

    private static Logger log = LogManager.getLogger(TaskController.class);

    private Scanner scanner;
    private Map<String, Runnable> methodMenu;
    private TaskService taskService;

    public TaskController() {
        taskService = new TaskServiceImpl();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", () -> taskService.countSentenceRepeatWord());
        methodMenu.put("2", () -> taskService.showSentenceByWordNumber());
        methodMenu.put("3", () -> taskService.interrogativeSentence(scanInt()));
        methodMenu.put("4", () -> taskService.replaceWordForLongestWord());
        methodMenu.put("5", () -> taskService.deleteWordByLength(scanInt()));
        methodMenu.put("6", () -> taskService.findUniqueWordInFirstSentence());
        methodMenu.put("7", () -> taskService.sortWordsByAlphabet());
        methodMenu.put("8", () -> taskService.sortVowelWordByConsonants());
        methodMenu.put("9", () -> taskService.sortByLetterCount(scanChar()));
        methodMenu.put("10", () -> taskService.swapWordInSentence(scanInt(), scanInt(), scanString()));
        methodMenu.put("11", () -> taskService.sortByLetterCountReverse(scanChar()));
        methodMenu.put("12", () -> taskService.deleteFirstLastLetterFromWord());
        methodMenu.put("13", () -> taskService.deleteSubstringBy(scanChar(), scanChar()));
        methodMenu.put("14", () -> taskService.sortWordsByVowelPercent());
    }

    public void getMethod(String command) {
        methodMenu.get(command).run();
    }

    private int scanInt() {
        scanner = new Scanner(System.in);
        log.info("Input number :");
        return scanner.nextInt();
    }

    private char scanChar() {
        scanner = new Scanner(System.in);
        log.info("Input char : ");
        return scanner.next().charAt(0);
    }

    private String scanString() {
        scanner = new Scanner(System.in);
        log.info("Input new String :");
        return scanner.next();
    }
}
